# Project 3

Web Programming with Python and JavaScript
Based off of Django tutorial from:
https://www.youtube.com/watch?v=z4USlooVXG0&list=PLLRM7ROnmA9F2vBXypzzplFjcHUaKWWP5

## Requirements
- Menu: Item  model has options to cover all items on Pinochio's menu.
- Adding Items: admin.py regsiters models to all adding of items to tables.
- Registration, Login, Logout: accounts allauth urls handles these actions.
    - Also have custom signup page to meet criteria.
- Shopping Cart: There is a shopping cart that is handled by the Orders model.
- Placing Order: Checkout method in views handles checkouts.
- Viewing Orders: Django admin allows viewing of orders table.
- Personal Touch: Stripe payment method.

## Details
### Orders
- admin.py: Add models to DJango Admin.
- forms.py: Create forms to add items to order and checkout.
- models.py: Create models to handle various website functions.
- urls.py: Add routes.
- views.py: Add functions for routes.
### Pizza
- settings.py: Setup django app.
- urls.py: Add routes.
### Static
- css: Add bootstrap.
- img: Add food photos.
- js: Add bootstrap js.
### Templates
- account:
    - login.html: Add bootstrap classes and csrf protection.
    - logout.html: Add bootstrap classes and csrf protection.
    - signup.html: Add bootstrap classes and csrf protection.
- base:
    - base.html: Base template.
    - footer.html: Footer.
    - navbar.html: Creates navbar.
    - scripts.html: Add bootstrap and jquery scripts.
- menu:
    - menu.html: Display all menu items.
    - product.html: Display info on individula item.
- checkout.html: Display checkout form.
- payment.html: Display page to pay with stripe.
