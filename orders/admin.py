from django.contrib import admin

# Register your models here.
from .models import *


admin.site.site_header = "Pinochio Admin"
admin.site.site_title = "Pinochio Admin Area"
admin.site.index_title = "Welcome to the Pinochio Admin Area"


admin.site.register(Item)
admin.site.register(FoodType)
admin.site.register(OrderItem)
admin.site.register(Toppings)
admin.site.register(Orders)
admin.site.register(Extras)
