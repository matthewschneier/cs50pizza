from django import forms
from localflavor.us.forms import USStateSelect, USZipCodeField
from .models import *


class OrderForm(forms.Form):
    toppings = Toppings.objects.values_list("abbr", "name")
    extras = Extras.objects.values_list("abbr", "name")
    quantity = forms.IntegerField(label="Quantity")
    size = forms.ModelChoiceField(
        label="Size", queryset=Sizes.objects.all()
    )
    toppings = forms.MultipleChoiceField(
        widget=forms.CheckboxSelectMultiple,
        choices=toppings
    )
    extras = forms.MultipleChoiceField(
        widget=forms.CheckboxSelectMultiple,
        choices=extras
    )


class CheckoutForm(forms.Form):
    paymentChoices = [
        ("S", "Stripe"),
        ("P", "PayPal")
    ]
    streetAddress = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "123 Main Street"}),
        label="Street Address", required=True
    )
    apartment = forms.CharField(
        widget=forms.TextInput(attrs={"placeholder": "Apartment or Suite"}),
        required=False
    )
    state = USStateSelect()
    zipcode = USZipCodeField()
    sameBillingAddress = forms.BooleanField(
        label="Same Billing Address?", widget=forms.CheckboxInput
    )
    payment = forms.ChoiceField(
        label="Payment Choices", widget=forms.RadioSelect, choices=paymentChoices
    )
