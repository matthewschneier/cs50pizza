# Generated by Django 2.0.3 on 2020-04-21 19:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0005_auto_20200421_1957'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Pasta',
        ),
        migrations.DeleteModel(
            name='Platters',
        ),
        migrations.DeleteModel(
            name='RegPizza',
        ),
        migrations.DeleteModel(
            name='Salads',
        ),
        migrations.DeleteModel(
            name='SicPizza',
        ),
        migrations.DeleteModel(
            name='Subs',
        ),
    ]
