# Generated by Django 2.0.3 on 2020-04-27 23:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0012_auto_20200427_1957'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='abbr',
            field=models.CharField(default='pizza', max_length=98),
        ),
        migrations.AddField(
            model_name='item',
            name='extraCheeseOption',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='item',
            name='hasToppings',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='item',
            name='name',
            field=models.CharField(default='pizza', max_length=99),
        ),
    ]
