from django.conf import settings
from django.db import models
from django.shortcuts import reverse
from localflavor.us.models import USStateField, USZipCodeField


class Sizes(models.Model):
    idSizes = models.AutoField(primary_key=True)
    abbr = models.CharField(max_length=2, unique=True)
    name = models.CharField(max_length=99, unique=True)

    def __unicode__(self):
        return self.name


class Toppings(models.Model):
    abbr = models.CharField(max_length=9, unique=True, default="A")
    name = models.CharField(max_length=30, unique=True, default="Cheese")

    def __str__(self):
        return self.name


class FoodType(models.Model):
    abbr = models.CharField(max_length=10, unique=True, default="regPizza")
    fullName = models.CharField(max_length=30, unique=True, default="Regular Pizza")

    def __str__(self):
        return self.fullName


class Item(models.Model):
    name = models.CharField(max_length=99, default="pizza")
    abbr = models.CharField(max_length=98, default="pizza")
    foodType = models.ForeignKey(FoodType, default=1, on_delete=models.CASCADE)
    smallPrice = models.DecimalField(max_digits=5, default=0.00, decimal_places=2)
    largePrice = models.DecimalField(max_digits=5, blank=True, null=True, decimal_places=2)
    hasExtras = models.BooleanField(default=False)
    description = models.CharField(max_length=999, default="Good food.")
    slug = models.SlugField(default="testProduct")
    hasToppings = models.BooleanField(default=False)
    maxToppings = models.IntegerField(blank=True, null=True)

    def get_absolute_url(self):
        return reverse("pinochio:menu", kwargs={
            "slug": self.slug
        })

    def getFoodType(self):
        return FoodType.objects.filter(id=self.id)[0]["abbr"]

    def getToppings(self):
        return Toppings.objects.all()

    def getExtras(self):
        return Extras.objects.all()

    def getRemoveFromCartURL(self):
        return reverse("pinochio:removeFromCart", kwargs={
            "slug": self.slug
        })

    def __str__(self):
        return self.name

class OrderItem(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True,
        on_delete=models.CASCADE)
    ordered = models.BooleanField(default=False)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    size = models.CharField(
        max_length=2,
        choices=[("Sm", "Small"), ("Lg", "Large")],
        default="Sm",
    )
    toppings = models.CharField(max_length=99, blank=True, null=True)
    extras = models.CharField(max_length=99, blank=True, null=True)
    quantity = models.IntegerField(default=1)

    def __str__(self):
        return f"{self.quantity} of {self.item.name}"

    def getTotalItemPrice(self):
        if self.size == "Lg":
            return self.quantity * self.item.largePrice
        return self.quantity * self.item.smallPrice


class Orders(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    startDate = models.DateTimeField(auto_now_add=True)
    orderDate = models.DateTimeField()
    total = models.DecimalField(default=0.00, max_digits=6, decimal_places=2)
    items = models.ManyToManyField(OrderItem)
    ordered = models.BooleanField(default=False)
    billingAddress = models.ForeignKey(
        "BillingAddress", on_delete=models.SET_NULL, blank=True, null=True
    )

    def __str__(self):
        return self.user.username

    def getTotal(self):
        total = sum([item.gelTotalItemPrice for item in self.items.all()])
        return total

class Extras(models.Model):
    abbr = models.CharField(max_length=9, unique=True, default="A")
    name = models.CharField(max_length=99, unique=True, default="Cheese")
    price = models.DecimalField(max_digits=5, decimal_places=2)

    def __str__(self):
        return self.name


class BillingAddress(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    streetAddress = models.CharField(max_length=200, default="123 Main Street")
    apartment = models.CharField(max_length=99, null=True, blank=True)
    zipcode = USZipCodeField()
    state = USStateField()
