from django import template
from orders.models import Orders

register = template.Library()


@register.filter
def cart_item_count(user):
    if user.is_authenticated():
        query = Orders.objects.filter(user=user, ordered=False)
        if query.exists():
            return query[0].items.count()
    return 0

