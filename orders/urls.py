from django.conf.urls import url
from django.urls import path
from .views import *


app_name = "pinochio"

urlpatterns = [
    url("/", home, name="home"),
    url(r"^menu/$", menu, name="menu"),
    path("checkout/", CheckoutView.as_view(), name="checkout"),
    path("order_summary/", OrderSummaryView.as_view(), name="orderSummary"),
    path("menu/<slug>", itemDetailView, name="product"),
    path("addToCart/<slug>/", addToCart, name="addToCart"),
    path("subtractFromCart/<slug>", subtractFromCart, name="subtractFromCart"),
    path("removeFromCart/<slug>", removeFromCart, name="removeFromCart"),
    path("payment/<option>/<id>", payment, name="payment"),
]
