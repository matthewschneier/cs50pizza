from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse, HttpResponseRedirect
from django.db.models import F
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.decorators import method_decorator
from django.utils.timezone import now
from django.views.generic import View
from .forms import CheckoutForm, OrderForm
from .models import *

import stripe


def home(request):
    return HttpResponseRedirect("/menu")


def itemDetailView(request, slug):
    "Show details of an individual item."
    form = OrderForm()
    item = Item.items.filter(slug=slug)[0]
    if request.method == "POST" and form.is_valid():
        # Make sure there aren't too many toppings.
        maxToppings = item["maxToppings"]
        toppings = ""
        if maxToppings:
            numToppings = 0
            for top in form.cleaned_data["toppings"]:
                if top.selected:
                    numToppings +=  1
                    toppings += f"{top}, "
        if numToppings > maxToppings:
            messages.error(request, "Too many toppings.")
            return render(
                request, "menu/product.html", {"item": item, "form": form}
            )
        # If item has extras add them.
        extras = ""
        if item["hasExtras"]:
            for extra in form.cleaned_data["toppings"]:
                if extra.selected:
                    extras += f"{extra}, "
        addToCart(
            request, slug, toppings, extras, form.cleanded_data["quantity"]
        )
        # Inform user item was added.
        messages.success(request, "Item added to cart.")
        return render(
            request, "menu/product.html", {"item": item, "form": form}
        )
    return render(request, "menu/product.html", {"item": item, "form": form})


def menu(request):
    "Show menu of all items."
    types = FoodType.objects.all()
    foods = {}
    for food in types:
        foods[food["id"]] = {
            "items": Item.objects.filter(FoodType=food["id"]),
            "abbr": food["abbr"],
            "fullName": food["fullName"],
        }
    toppings = Toppings.objects.all()
    extras = Extras.objects.all()
    return render(request, "menu/menu.html", {
        "types": types,
        "foods": foods,
        "toppings": toppings,
        "extras": extras,
    })


class OrderSummaryView(LoginRequiredMixin, View):
    "Show summary of cutomer's order."
    def get(self, *args, **kwargs):
        try:
            order = Orders.objects.get(user=self.request.user, ordered=False)
            return render(self.request, "account/order_summary.html", {"object": order})
        except ObjectDoesNotExist:
            messages.error(self.request, "You do not have an active order.")
            return redirect("/")


@login_required
def addToCart(request, slug, toppings, extras, quantity):
    "Add item to customer's cart."
    item = get_object_or_404(Item, slug=slug)
    orderItem, created = OrderItem.objects.get_or_create(
        item=item,
        user=request.user,
        ordered=False,
        toppings=toppings,
        extras=extras,
        quantity=quantity,
    )
    # Check if customer has existing order, if so add item to it, if not create new one.
    order = Orders.objects.filter(user=request.user, ordered=False)
    if order.exists():
        order = order[0]
        # If item already in cart, increase quantity.
        if order.items.filter(item__slug=item.slug).exists():
            num = order.items.filter(
                item__slug=item.slug
            ).quantity
            # F function prevents race condition problems.
            orderItem.quantity += F("quantity") + num
            orderItem.save()
            messages.info(request, "This item was added to your cart.")
            return redirect("pinochio:orderSummary")
        else:
            order.items.add(orderItem)
    else:
        orderDate = now()
        order = Orders.objects.create(
            user=request.user, orderDate=orderDate
        )
        order.items.add(orderItem)
    return


@login_required
def subtractFromCart(request, slug):
    "Decrease qunaity of item in cart."
    # Get item or return 404 error.
    item = get_object_or_404(Item, slug=slug)
    order = Orders.objects.filter(user=request.user, ordered=False)
    # Check customer has order.
    if order.exists():
        order = order[0]
        # Check item is in cart.
        if order.items.filter(item__slug=item.slug).exists():
            orderItem = OrderItem.objects.filter(
                item=item,
                user=request.user,
                ordered=False
            )[0]
            if orderItem.quantity > 1:
                orderItem.quantity = F("quantity") - 1
                orderItem.save()
            else:
                order.items.remove(orderItem)
            messages.info(request, "This item was removed from your cart.")
            return redirect("pinochio:orderSummary")
        else:
            messages.info(request, "This item was not in your cart.")
            return redirect("pinochio:product", slug=slug)
    else:
        messages.info(request, "You do not have an active order.")
        return redirect("pinochio:product", slug=slug)


@login_required
def removeFromCart(request, slug):
    "Remove item from cart."
    item = get_object_or_404(Item, slug=slug)
    order = Orders.objects.filter(user=request.user, ordered=False)
    if order.exists():
        order = order[0]
        if order.items.filter(item__slug=item.slug).exists():
            orderItem = OrderItem.objects.filter(
                item=item,
                user=request.user,
                ordered=False
            )[0]
            order.items.remove(orderItem)
            messages.info(request, "This item was removed from your cart.")
            return redirect("pinochio:orderSummary")
        else:
            messages.info(request, "This item was not in your cart.")
            return redirect("pinochio:product", slug=slug)
    else:
        messages.info(request, "You do not have an active order.")
        return redirect("pinochio:product", slug=slug)


class CheckoutView(LoginRequiredMixin, View):
    "Checkout"
    def get(self, *args, **kwargs):
        "Display order before checkout."
        form = CheckoutForm()
        # Check if customer has order.
        try:
            order = Orders.objects.get(user=self.request.user, ordered=False)
            return render(self.request, "checkout.html", {"cart": order[0], "form": form})
        except ObjectDoesNotExist:
            messages.error(self.request, "You do not have an active order.")
            return redirect("/menu")
    def post(self, *args, **kwargs):
        "Checkout customer."
        form = CheckoutForm(self.request.POST or None)
        # Check if customer has order.
        try:
            order = Orders.objects.get(user=self.request.user, ordered=False)
        except ObjectDoesNotExist:
            messages.error(self.request, "You do not have an active order.")
            return redirect("/menu")
        if form.is_valid():
            streetAddress = form.cleaned_data.get("steetAddress")
            apartment = form.cleanded_data.get("apartment")
            zipcode = form.cleaned_data.get("zipcode")
            payment = form.cleanded_data.get("payment")
            state = form.cleanded_data.get("state")
            billingAddress = BillingAddress(
                user=self.request.user,
                streetAddress=streetAddress,
                apartment=apartment,
                zipcode=zipcode,
                state=state
            )
            billingAddress.save()
            order[0].billingAddress = billingAddress
            order[0].save()
            return redirect(f"/payment/{payment}/{order[0].id}")
        # If checkout fails, inform user.
        messages.warning(self.request, "Failed Checkout.")
        return redirect("pinochio:checkout")


@login_required
def payment(request, payment, orderID):
    "Handle stripe payment."
    key = settings.STRIPE_PUBLISHABLE_KEY
    order = Orders.objects.get(user=request.user, id=orderID)
    total = order.getTotal()
    if request.method == "POST":
        charge = stripe.Charge.create(
            amount=total,
            currency="usd",
            description="Pinochios",
            source=request.POST["stripeToken"]
        )
    return render(request, "payment.html", {"key": key, "amount": total})

